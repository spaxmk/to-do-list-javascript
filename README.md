# TO-DO List sa Pie diagram  ( Ispitni projekat ) 

Koristeći JavaScript napravljen je modul za vođenje evidencije zadataka. U koliko
dobijemo određen projekat za izradu, uzećemo za primer izradu neke veb aplikacije moja se
sastoji od više delova. Tim ljudi koji razvijaju zadatu aplikaciju mogu podeliti izradu
aplikacije u desetine segmenata, odnosno zadataka. Kad tim počne da radi zadate zadatke
mogu da vode evidenciju o urađenosti zadataka i postotku urađenosti aplikacije.  

![Page - page Screenshot](/img/imgReadme/FullPage.png)

Na dnu imamo polje za unos teksta, pritiskom na dugme sa leve strane ili pritiskom na
enter na samoj tastaturi ubacujemo tekst u tabelu. Zadatak koji unesemo dobija sa svoje leve
strane kružić za označavanje. Prilikom dodavanja zadatka, kružić dobija predefinisan
prazan kružić koji prestavlja da takav zadatak nije izvršen. Nakon izvršetka upisanog
zadatka dovoljno je kliknuti na prazan kružić kako bi on dobio kvačicu u smuslu da je taj
zadatak izvršen, i samim tim tekst ce postati svetliji i precrtan.

Brisanje zadatka je moguće odabirom željenog zadatka, i na kraju, odnosno sa njegove desne
strane se nalazi kantica koja označava njegovo brisanje.

Svakom promenom u tabeli, unosom novog zadatka, promenom oznake ili brisanjem
postojećeg zadatka, tabela se čuva u memoriji. Samim tim prilikom zatvaranja strane, i
pokretanjem iste opet, tabela se učitava iz memorije.

Celu tabalu je moguće obrisati klikom na kružne strelice u vrhu modula za zadatke. Na
taj način brišemo kompletnu tabelu iz memorije.

Gijagramom je prikazan broj i postotak zadataka koji su izvršeni i zadataka koji nisu
izvršeni. Prolaskom kroz listu zadataka računa se broj izvršenih i neizvršenih
zadataka i na osnovu tog broja se formira dijagram sa postotkom izrade zadatog zadatka. Za
prikaz dijagrama potrebno je osvežiti stanicu kako bi se učitale sve promene u tabeli